#  Augest 17 2021 at 16:00pm-17:00pm GMT+8

## Agenda

- OH BlockChain SIG例会[会议录制链接](https://meeting.tencent.com/v2/cloud-record/share?id=159dd1ef-30e2-4846-8a9b-2fc7e2e1482e&from=3) 


##与会人员
- 李凯
- 房隆军
- 梁克雷
- 邱炜伟
- [jiafeicheng](https://gitee.com/15335180944)
- [haisong2](https://gitee.com/haisong2)
- [Lee](https://gitee.com/www.lee.com)
- [platformdepartment](https://gitee.com/platformdepartment)
- [lingfengbao](https://gitee.com/lingfengbao)
- [xkwang1228](https://gitee.com/xkwang1228)
- [jerryma0912](https://gitee.com/jerryma0912)
- [shangxuan_hz](https://gitee.com/shangxuan_hz)
- [zhao_kuo](https://gitee.com/zhao_kuo)




## 目标

1. OH Blockchain SIG成立概况及成员介绍

2. OH Blockchain SIG定位、工作范围、实现方式、今年工作计划说明




## 记录

### 1. OH Blockchain SIG整体介绍 by 趣链-尚璇

### 2. 代码规范说明 by - 软件所-李凯老师

- OH代码规范问题文档说明：存在但比较零散，后期会统一汇总，供大家参考。

- 会议纪要说明：有专门的content模板，要将会议纪要和录屏一并上传到项目指定位置中。

- 代码标签说明

- 问题issue提交流程：任务更新（进展-阻塞点-下一步计划）

- 毕业代码 - 检查工具（sig- content-签署DCO）

### 3. 应用场景

- 区块链+数据价值归属：通过区块链联合多家中小企业形成联盟，数据共享多方共赢。by 解放号-贾老师

- 区块链+大安防领域：数据安全隐私保护，解决多方利益冲突主体不可信的问题。趣链-李世敬

- 区块链+碳中和：解决资源不匹配的问题，可以从碳中和市场标准、示范案例切入。by 中央财经-陈老师



# 后续工作

- 
- 


