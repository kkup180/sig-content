# 	Content

简体中文|[English](./README.en.md)

说明：本SIG的内容遵循OpenHarmony的PMC管理章程 [README](/zh/pmc.md)中描述的约定

## SIG组工作目标和范围

### 工作目标

为SIG仓提供文档、会议纪要及其他文档提供仓库，同时作为各SIG组的索引页

### 工作范围

* 存放各SIG组每次例会后产生的会议纪要、会议材料，其中会议纪要模板请参考[链接](https://gitee.com/openharmony/community/tree/master/sig/sig-template/meetings)
* 提供各SIG组链接索引
* 为社区宣传提供可用的素材

## 本SIG组成员

### Leader

- [@minglonghuang ](https://gitee.com/minglonghuang)

### Committers列表

- [@kevenNO1](https://gitee.com/kevenNO1)
- [@jony_code](https://gitee.com/jony_code)
- [@zhao_xiuxiu](https://gitee.com/zhao_xiuxiu)
- [@yu_jia_geng](https://gitee.com/yu_jia_geng)
- [@nicolaswang](https://gitee.com/nicolaswang)
- [@duzc2](https://gitee.com/duzc2)

###  各SIG组列表

1. [sig-devboard](https://gitee.com/openharmony/community/blob/master/sig/sig-devboard/sig_devboard.md)
2. [sig-dllite-micro](https://gitee.com/openharmony/community/blob/master/sig/sig-dllite-micro/sig_dllite_micro.md)
3. [sig-openblock](https://gitee.com/openharmony/community/blob/master/sig/sig-openblock/sig_openblock.md)
4. [sig-riscv](https://gitee.com/openharmony/community/blob/master/sig/sig-riscv/sig-riscv.md)
5. [sig-python](https://gitee.com/openharmony/community/blob/master/sig/sig-python/sig-python_cn.md)
6. [sig-linkboy](https://gitee.com/openharmony/community/blob/master/sig/sig-linkboy/sig_linkboy_cn.md)
7. [sig-systemapplication]( https://gitee.com/openharmony/community/blob/master/sig/sig-systemapplications/sig_systemapplications_cn.md)

### 索引
1. [sig维护框架](./Infrastructure/docs/manual/sig数据维护框架.md)
2. [sig的总入口](https://gitee.com/openharmony/community/tree/master/sig)
3. [gitee 提PR操作指导](./Infrastructure/docs/manual/gitee提pr常规操作.md)
4. [代码/文档规范](https://gitee.com/openharmony/docs/tree/master/zh-cn/contribute)

7. [开源合规检查工具](https://gitee.com/openharmony-sig/tools_oat)
8. [对于主线已有库的维护方法](./Infrastructure/docs/manual/patch管理/针对修改已有库需求的管理方案.md)

### 联系方式(可选)

- 欢迎[订阅](https://lists.openatom.io/postorius/lists/dev.openharmony.io/)邮件列表：dev@openharmony.io
- Slack群组：NONE
- 微信群：NONE
